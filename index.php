<?php
    $cabecera= file_get_contents("template/cabecera.html");
    echo $cabecera;
?>
  <body>
    <h1>Ejemplo</h1>
    <div class="form-group">
    <label for="color">Color de la linea</label>
    <select class="form-control" id="color">
        <option value="RED">Rojo</option>
        <option value="BLUE">Azul</option>
        <option value="BLACK">Negro</option>
    </select>
  </div>
<?php
    $pie= file_get_contents("template/pie.html");
    echo $pie;
?>
  </body>
</html>